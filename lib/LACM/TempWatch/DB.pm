# -*- perl -*-
#
# LACM::TempWatch::DB - database connection and helpers
#
package LACM::TempWatch::DB;

use strict;
use warnings;

use Carp;
use DBI;
use LACM::TempWatch::Config     qw($Config);

###############################################################################
# BEGIN user-configurable section

# END   user-configurable section
###############################################################################

# Program name of our caller
(our $ME = $0) =~ s|.*/||;

# For non-OO exporting of code, symbols
our @ISA         = qw(Exporter);
our @EXPORT      = qw();
our @EXPORT_OK   = qw();
our %EXPORT_TAGS =   (all => \@EXPORT_OK);


###################
#  new  #  Initialize database connection
###################
sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $db    = shift;                     # in: database name

    my $handle = sprintf("dbi:%s:dbname=%s",
                         $Config->required($db, 'provider'),
                         $Config->required($db, 'dbname'));

    for my $field (qw(host port)) {
        if (my $val = $Config->optional($db, $field)) {
            $handle .= ";$field=$val";
        }
    }

    my $dbh = DBI->connect(
        $handle,
        $Config->optional('message_db', 'username'),
        $Config->optional('message_db', 'password'),
        { mysql_enable_utf8 => 1 },
    );

    my $self = { DBH => $dbh };

    return bless $self, $class;
}

########
#  do  #  Kind of like DBI->do, but my own handling for select/insert
########
sub do {
    my $self = shift;
    my $sql  = shift;

    # FIXME: how do we get verbose/debug/NOT from $main?
#    if ($verbose || $debug || $NOT) {
#        local $" = ', ';
#        print "$sql$NOT\n --> (@_)\n";
#        return if $NOT;
#    }

    my $sth = $self->{DBH}->prepare($sql);
    $sth->execute(@_)
        or die "failed: $sql: " . $self->{DBH}->errstr;

    my @results;
    if ($sql =~ /^SELECT\s/) {
        while (my $href = $sth->fetchrow_hashref) {
            push @results, $href;
        }
    }
    elsif ($sql =~ /^INSERT\s/) {
        # This is an INSERT statement. Return the generated sequence id.
        push @results, $self->{DBH}->last_insert_id("","","","");
    }
    elsif ($sql =~ /^UPDATE\s/) {
        push @results, $sth->rows;
    }

    return wantarray ? @results : $results[0];
}

# Let caller pretend we're a DBI instance
sub prepare {
    my $self = shift;
    return $self->{DBH}->prepare(@_);
}
sub errstr {
    my $self = shift;
    return $self->{DBH}->errstr(@_);
}

1;


###############################################################################
#
# Documentation
#

=head1	NAME

FIXME - FIXME

=head1	SYNOPSIS

    use Fixme::FIXME;

    ....

=head1	DESCRIPTION

FIXME fixme fixme fixme

=head1	CONSTRUCTOR

FIXME-only if OO

=over 4

=item B<new>( FIXME-args )

FIXME FIXME describe constructor

=back

=head1	METHODS

FIXME document methods

=over 4

=item	B<method1>

...

=item	B<method2>

...

=back


=head1	EXPORTED FUNCTIONS

=head1	EXPORTED CONSTANTS

=head1	EXPORTABLE FUNCTIONS

=head1	FILES

=head1	SEE ALSO

L<Some::OtherModule>

=head1	AUTHOR

Ed Santiago <ed@edsantiago.com>

=cut
